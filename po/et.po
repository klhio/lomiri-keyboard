# Estonian translation for lomiri-keyboard
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the lomiri-keyboard package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-keyboard\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-18 19:28+0000\n"
"PO-Revision-Date: 2023-01-05 10:42+0000\n"
"Last-Translator: Anonymous <noreply@weblate.org>\n"
"Language-Team: Estonian <https://hosted.weblate.org/projects/lomiri/"
"lomiri-keyboard/et/>\n"
"Language: et\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2017-04-07 06:31+0000\n"

#: ../qml/ActionsToolbar.qml:56
#, fuzzy
msgid "Select All"
msgstr "Select All"

#: ../qml/ActionsToolbar.qml:57
msgid "Redo"
msgstr ""

#: ../qml/ActionsToolbar.qml:58
msgid "Undo"
msgstr ""

#: ../qml/ActionsToolbar.qml:73
msgid "Paste"
msgstr ""

#: ../qml/ActionsToolbar.qml:74
#, fuzzy
msgid "Copy"
msgstr "Copy"

#: ../qml/ActionsToolbar.qml:75
msgid "Cut"
msgstr ""

#: ../qml/FloatingActions.qml:62
msgid "Done"
msgstr ""

#: ../qml/Keyboard.qml:351
msgid "Swipe to move selection"
msgstr ""

#: ../qml/Keyboard.qml:352
msgid "Swipe to move cursor"
msgstr ""

#: ../qml/Keyboard.qml:352
msgid "Double-tap to enter selection mode"
msgstr ""

#: ../qml/keys/LanguageMenu.qml:106
msgid "Settings"
msgstr "Seaded"

#: ../qml/keys/languages.js:19
msgid "Arabic"
msgstr "Araabia"

#: ../qml/keys/languages.js:20
msgid "Azerbaijani"
msgstr "Ažerbaidžaani"

#: ../qml/keys/languages.js:21
#, fuzzy
#| msgid "Hungarian"
msgid "Bulgarian"
msgstr "Ungari"

#: ../qml/keys/languages.js:22
msgid "Bosnian"
msgstr "Bosnia"

#: ../qml/keys/languages.js:23
msgid "Catalan"
msgstr ""

#: ../qml/keys/languages.js:24
msgid "Czech"
msgstr "Tšehhi"

#: ../qml/keys/languages.js:25
msgid "Danish"
msgstr "Taani"

#: ../qml/keys/languages.js:26
msgid "German"
msgstr "Saksa"

#: ../qml/keys/languages.js:27
msgid "Emoji"
msgstr ""

#: ../qml/keys/languages.js:28
msgid "Greek"
msgstr "Kreeka"

#: ../qml/keys/languages.js:29
msgid "English"
msgstr "Inglise"

#: ../qml/keys/languages.js:30
msgid "Esperanto"
msgstr ""

#: ../qml/keys/languages.js:31
msgid "Spanish"
msgstr "Hispaania"

#: ../qml/keys/languages.js:32
msgid "Persian"
msgstr ""

#: ../qml/keys/languages.js:33
msgid "Finnish"
msgstr "Soome"

#: ../qml/keys/languages.js:34
msgid "French"
msgstr "Prantsuse"

#: ../qml/keys/languages.js:35
#, fuzzy
#| msgid "French"
msgid ""
"French\n"
"(Swiss)"
msgstr "Prantsuse"

#: ../qml/keys/languages.js:36
msgid "Scottish Gaelic"
msgstr ""

#: ../qml/keys/languages.js:37
msgid "Hebrew"
msgstr ""

#: ../qml/keys/languages.js:38
msgid "Croatian"
msgstr ""

#: ../qml/keys/languages.js:39
msgid "Hungarian"
msgstr "Ungari"

#: ../qml/keys/languages.js:40
msgid "Icelandic"
msgstr ""

#: ../qml/keys/languages.js:41
msgid "Italian"
msgstr "Itaalia"

#: ../qml/keys/languages.js:42
msgid "Japanese"
msgstr ""

#: ../qml/keys/languages.js:43
msgid "Lithuanian"
msgstr ""

#: ../qml/keys/languages.js:44
msgid "Latvian"
msgstr ""

#: ../qml/keys/languages.js:45
msgid "Korean"
msgstr ""

#: ../qml/keys/languages.js:46
msgid "Dutch"
msgstr ""

#: ../qml/keys/languages.js:47
msgid "Norwegian"
msgstr ""

#: ../qml/keys/languages.js:48
msgid "Polish"
msgstr "Poola"

#: ../qml/keys/languages.js:49
msgid "Portuguese"
msgstr "Portugali"

#: ../qml/keys/languages.js:50
msgid "Romanian"
msgstr "Rumeenia"

#: ../qml/keys/languages.js:51
msgid "Russian"
msgstr "Vene"

#: ../qml/keys/languages.js:52
msgid "Slovenian"
msgstr "Sloveenia"

#: ../qml/keys/languages.js:53
msgid "Serbian"
msgstr ""

#: ../qml/keys/languages.js:54
msgid "Swedish"
msgstr ""

#: ../qml/keys/languages.js:55
msgid "Turkish"
msgstr ""

#: ../qml/keys/languages.js:56
msgid "Ukrainian"
msgstr "Ukraina"

#: ../qml/keys/languages.js:57
msgid ""
"Chinese\n"
"(Pinyin)"
msgstr ""

#: ../qml/keys/languages.js:58
msgid ""
"Chinese\n"
"(Chewing)"
msgstr ""
